// helloword.cpp: ������� ���� �������.

//#include "stdafx.h"

//using namespace System;
#include <stdio.h>
#include <ctype.h>
//#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <locale.h>
#include <assert.h>
#include <stdbool.h>

//int j = 0, nUnicWords = 0, nTotalWords = 0;
//char **cFirstLevel[255];
#define sc_nSizeOfPageInStructOfIndex 255
static const int sc_nBufferMemory = 1024;
static const int sc_nPageHeavy = 1024 * 1;
static char s_cDelim[] = " ,.-+<>_=#$%^&*()@<>\'\"\n/[]{}|\r\t!\?:1234567890\xb7";

//static char s_cDelim[] = " ._=";
//int open(char *name, int flags, int perms);
//int read(int fd, char *buff, int n);

struct stPageData
{
	struct stPageData *m_pstPreviousPage;
	char *m_pcEndData;
};

struct stIndexPage
{
	struct stIndexPage *m_pstPreviousIndexPage;
	int m_nIndexAmmount;
};

struct stPageData *createPageData(struct stPageData **pstLastestPage)
{
	struct stPageData *pstTempCreate = malloc(sc_nPageHeavy);

	pstTempCreate->m_pstPreviousPage = *pstLastestPage;
	pstTempCreate->m_pcEndData = (char *)(pstTempCreate + 1);
	*pstLastestPage = pstTempCreate;

	return pstTempCreate;
}

struct stIndexPage *createIndexPage(struct stIndexPage **pstLastestCreatedIndexPage)
{
	struct stIndexPage *pstIndexTempCreate = malloc(sizeof(struct stIndexPage) + sc_nSizeOfPageInStructOfIndex * (sizeof (struct stIndexPage *)));

	pstIndexTempCreate->m_pstPreviousIndexPage = *pstLastestCreatedIndexPage;
	pstIndexTempCreate->m_nIndexAmmount = 1;

	*pstLastestCreatedIndexPage = pstIndexTempCreate;
	return pstIndexTempCreate;
}

void eraseMemoryIndex(struct stIndexPage *pstLastestCreatedIndexPage)
{
	while (pstLastestCreatedIndexPage != NULL)
	{
		struct stIndexPage *pstTempIndex = pstLastestCreatedIndexPage->m_pstPreviousIndexPage;
		free(pstLastestCreatedIndexPage);
		pstLastestCreatedIndexPage = pstTempIndex;
	}
}

void eraseMemoryData(struct stPageData *pstLastestPage)
{
	while (pstLastestPage != NULL)
	{
		struct stPageData *pstTemp = pstLastestPage->m_pstPreviousPage;
		free(pstLastestPage);
		pstLastestPage = pstTemp;
	}
}

bool search(char *pcString, int nSizeWord, struct stIndexPage **ppstFirstLevelAdress, struct stIndexPage **ppstSecondLevelAdress, char **ppcThirdLevelAdress, struct stIndexPage **pstLastestCreatedIndexPage)
{
//	if (strcmp(pcString, "�") == 0)
//		printf("%s", pcString);

	if (*ppstFirstLevelAdress == NULL)
		return false;

	struct stIndexPage **pstSecondLevel;
	pstSecondLevel = (struct stIndexPage **)(*ppstFirstLevelAdress + 1);
	int i;
	/*
	for (i = 1; i <= sc_nSizeOfPageInStructOfIndex; i = i << 1)
	{
		if ((*ppstFirstLevelAdress)->m_nIndexAmmount <= i)
			break;
	}
	
	if ((*ppstFirstLevelAdress)->m_nIndexAmmount == 1)
		*ppstSecondLevelAdress = *pstSecondLevel;
	else
	{
		int nStarterI = i >> 1;
		i = i >> 1;

		for (int j = 2; nStarterI / j >= 1; j = j << 1)
		{
			*ppstSecondLevelAdress = pstSecondLevel[i - 1];
			int nCompareStrRezult = strncmp(*((char **)(*ppstSecondLevelAdress + 1)), pcString, nSizeWord);
			
			if (nCompareStrRezult == 0)
				return true;

			if (nCompareStrRezult < 0)
			{
				i = i + nStarterI / j;
				continue;
			}

			i = i - nStarterI / j;
		}
	}
	*/
	int nUpper = (*ppstFirstLevelAdress)->m_nIndexAmmount + 1;
	int nLower = 0;

	if (nUpper == 2)
		*ppstSecondLevelAdress = *pstSecondLevel;
	else
	{
		for (; nUpper - nLower > 1;)
		{
			int i = nLower + ((nUpper - nLower) >> 1);
			*ppstSecondLevelAdress = pstSecondLevel[i - 1];
			int nCompareStrRezult = strncmp(*((char **)(*ppstSecondLevelAdress + 1)), pcString, nSizeWord);

			if (nCompareStrRezult > 0)
			{
				nUpper = i;
				continue;
			}

			if (nCompareStrRezult == 0)
				return true;

			nLower = i;
		}

		if (nLower >= 1)
			*ppstSecondLevelAdress = pstSecondLevel[nLower - 1];
	}
	i = 0;
	char **pstThirdLevel = (char **)(*ppstSecondLevelAdress + 1);

/*	for (i = 1; i <= sc_nSizeOfPageInStructOfIndex; i = i << 1)
	{
		if ((*ppstSecondLevelAdress)->m_nIndexAmmount <= i)
			break;
	}
	if ((*ppstSecondLevelAdress)->m_nIndexAmmount == 1)
		*ppcThirdLevelAdress = pstThirdLevel[0];

	int nStarterI = i >> 1;
//	i = nStarterI;

	for (int j = 2; nStarterI / j >= 1; j = j << 1)
	{
		if (i > (*ppstSecondLevelAdress)->m_nIndexAmmount)
			i = i - nStarterI / j;
		else
		{
			*ppcThirdLevelAdress = pstThirdLevel[i - 1];
			int nCompareStrRezult = strncmp(*ppcThirdLevelAdress, pcString, nSizeWord);

			if (nCompareStrRezult == 0)
				return true;

			if (nCompareStrRezult < 0)
			{
				i = i + nStarterI / j;
				continue;
			}

			i = i - nStarterI / j;
		}
	}
*/
	int nLowerBorder = 0;
	int nUpperBorder = ((*ppstSecondLevelAdress)->m_nIndexAmmount) + 1;

	if (nUpperBorder == 2)
	{
		*ppcThirdLevelAdress = *pstThirdLevel;
		int nCompareStrRezult = strncmp(*ppcThirdLevelAdress, pcString, nSizeWord);

		if (nCompareStrRezult == 0)
			return true;
		if (nCompareStrRezult < 0)
			*ppcThirdLevelAdress = pstThirdLevel + 1;

		return false;
	}

	for (; (nUpperBorder - nLowerBorder > 1);)
	{
//		if (strcmp(pcString, "Game") == 0)
//			printf("True");

		int i = nLowerBorder + ((nUpperBorder - nLowerBorder) >> 1);
		*ppcThirdLevelAdress = pstThirdLevel[i - 1];
		int nCompareStrRezult = strncmp(*ppcThirdLevelAdress, pcString, nSizeWord);

		if (nCompareStrRezult < 0)
		{
			nLowerBorder = i;
			continue;
		}

		if (nCompareStrRezult == 0)
			return true;

		nUpperBorder = i;
	}

	*ppcThirdLevelAdress = pstThirdLevel[nUpperBorder - 1];

/*	if (nUpperBorder == (*ppstSecondLevelAdress)->m_nIndexAmmount + 1)
		*ppcThirdLevelAdress = pstThirdLevel + nUpperBorder - 1;
	if (nLowerBorder == 0)
		*ppcThirdLevelAdress = *pstThirdLevel;
*/
	return false;
}

struct stIndexPage *split(size_t szStrSize, struct stIndexPage **ppstFirstLevelAdress, struct stIndexPage **ppstSecondLevelAdress, 
							struct stIndexPage **ppstLastestCreatedIndexPage, struct stIndexPage **ppcThirdLevelAdress, int **nOffset)
{
	if ((*ppstFirstLevelAdress)->m_nIndexAmmount >= sc_nSizeOfPageInStructOfIndex)
	{
		printf("���������� ������� ���� ����� %d ��������� %d", szStrSize, sc_nSizeOfPageInStructOfIndex * sc_nSizeOfPageInStructOfIndex);
		return NULL;
	}

	struct stIndexPage *pstTempCreate = createIndexPage(ppstLastestCreatedIndexPage);
	pstTempCreate->m_nIndexAmmount = sc_nSizeOfPageInStructOfIndex / 2;
	(*ppstSecondLevelAdress)->m_nIndexAmmount = sc_nSizeOfPageInStructOfIndex / 2 + sc_nSizeOfPageInStructOfIndex % 2;

	memcpy(pstTempCreate + 1, (char **)(*ppstSecondLevelAdress + 1) + sc_nSizeOfPageInStructOfIndex / 2 + sc_nSizeOfPageInStructOfIndex % 2, sizeof(int) * sc_nSizeOfPageInStructOfIndex / 2);
	*nOffset = *ppcThirdLevelAdress - (char **)(*ppstSecondLevelAdress + 1);

	struct stIndexPage **pstCell = (struct stIndexPage **)(*ppstFirstLevelAdress + 1);
	int i;

	for (i = (*ppstFirstLevelAdress)->m_nIndexAmmount; (pstCell + i - 1 != *ppstSecondLevelAdress) && (i >= 1); i--)
		pstCell[i] = pstCell[i - 1];

	pstCell[i + 1] = pstTempCreate;
	(*ppstFirstLevelAdress)->m_nIndexAmmount = (*ppstFirstLevelAdress)->m_nIndexAmmount + 1;

	return pstTempCreate;
}

void paste(struct stIndexPage **pstFirstLevelAdress, struct stIndexPage **ppstSecondLevelAdress, char **ppcThirdLevelAdress, char *pcString,
			struct stIndexPage **ppstLastestCreatedIndexPage, struct stPageData **ppstLastestPage)
{
	assert(ppstLastestPage != NULL);
	assert(*ppstLastestPage != NULL);
	assert((*ppstLastestPage)->m_pcEndData != NULL);
	assert((pstFirstLevelAdress != NULL) && ((ppstSecondLevelAdress != NULL) || (ppcThirdLevelAdress != NULL)));
	assert(pcString != NULL);

	if ((ppstLastestPage == NULL) || ((ppstLastestPage != NULL) && (*ppstLastestPage != NULL) && ((*ppstLastestPage)->m_pcEndData == NULL)))
	{
		printf("�� ��������� �������� ��������� m_pcEndData\n");
		return;
	}

	size_t szStrSize = strlen(pcString);

// �������� ���������� ��������  ������ � �� ���������
	if  ((*ppstLastestPage)->m_pcEndData + szStrSize >= (char *)*ppstLastestPage + sc_nPageHeavy)
		*ppstLastestPage = createPageData(ppstLastestPage);

// ������� ����� � PageData
	char *pcPreviousPointerEndData = (*ppstLastestPage)->m_pcEndData;
	strcpy_s((*ppstLastestPage)->m_pcEndData, (char *)*ppstLastestPage + sc_nPageHeavy - (*ppstLastestPage)->m_pcEndData, pcString);
	(*ppstLastestPage)->m_pcEndData = (*ppstLastestPage)->m_pcEndData + szStrSize;

// TODO: �������� ���� �������
	if ((*pstFirstLevelAdress == NULL) || ((*pstFirstLevelAdress)->m_nIndexAmmount == 0))
	{
		*pstFirstLevelAdress = createIndexPage(ppstLastestCreatedIndexPage);
		*ppstSecondLevelAdress = createIndexPage(ppstLastestCreatedIndexPage);
		*((struct stIndexPage **)(*pstFirstLevelAdress + 1)) = *ppstSecondLevelAdress;
		*(char **)(*ppstSecondLevelAdress + 1) = pcPreviousPointerEndData;
		*ppcThirdLevelAdress = *((char **)(*ppstSecondLevelAdress + 1));
		return;
	}

	if ((*ppstSecondLevelAdress)->m_nIndexAmmount >= sc_nSizeOfPageInStructOfIndex)
	{
		int nOffset;
		if (&ppcThirdLevelAdress > (char **)(*ppstSecondLevelAdress + 1) + sc_nSizeOfPageInStructOfIndex / 2 + 1)
		{
			ppstSecondLevelAdress = split(szStrSize, pstFirstLevelAdress, ppstSecondLevelAdress, ppstLastestCreatedIndexPage, ppcThirdLevelAdress, &nOffset);
			ppcThirdLevelAdress = (char **)(*ppstSecondLevelAdress + 1) + nOffset;
		}

		else
			split(szStrSize, pstFirstLevelAdress, ppstSecondLevelAdress, ppstLastestCreatedIndexPage, ppcThirdLevelAdress, &nOffset);
	}

	char **pcCell = (char **)(*ppstSecondLevelAdress + 1);
	int i = (*ppstSecondLevelAdress)->m_nIndexAmmount;

// ����� �������� ��� �������
		while ((*(pcCell + i) != *ppcThirdLevelAdress) && (i >= 1))
		{
			pcCell[i] = pcCell[i - 1];
			i--;
		} 

	pcCell[i] = pcPreviousPointerEndData;
	(*ppstSecondLevelAdress)->m_nIndexAmmount = (*ppstSecondLevelAdress)->m_nIndexAmmount + 1;
}

main(int argc, char *argv[])
{
	size_t szResult;
	char *pcTmp;

	int nTotalWords = 0;
	int nUnicWords = 0;
	struct stIndexPage *cFirstLevel[sc_nSizeOfPageInStructOfIndex];
	memset(&cFirstLevel[0], 0, sc_nSizeOfPageInStructOfIndex * sizeof(struct stIndexPage *));
	struct stIndexPage *pstSecondLevelAdress = NULL;
	char *ppcThirdLevelAdress = NULL;
	struct stPageData *pstLastestPage = NULL;
	struct stIndexPage *pstLastestCreatedIndexPage = NULL;
	pstLastestPage = createPageData(&pstLastestPage);
	setlocale(LC_ALL, "rus");
/*	int i;

	for (i = 1; i < argc; i++)
		printf("%s%s", argv[i], (i < argc - 1) ? " " : "");
*/

	FILE *pFile;
	int nError;
	nError = fopen_s(&pFile, argv[1], "rb");

	if (nError == 0)
		printf("File was opened\n");
	else
	{
		printf("File wasn't opened\n");
		return;
	}

	char *pcBorders = malloc((sc_nBufferMemory + 1) * sizeof(char));
	char *pcStartRead = pcBorders;
	int nRealBufferSize = sc_nBufferMemory;
	int nWordLen = 0;
	bool blRetryFlag = true;

	do
	{
		szResult = fread(pcStartRead, sizeof(char), nRealBufferSize, pFile);
		blRetryFlag = szResult == nRealBufferSize;
		nRealBufferSize = sc_nBufferMemory;
		pcStartRead[szResult] = '\0';
		char *pcString = strtok_s(pcBorders, s_cDelim, &pcTmp);
		
		
		while (pcString != NULL)
		{
			nWordLen = strlen(pcString);
			for (int i = 0; i < nWordLen; i++)
				pcString[i] = tolower(pcString[i]);

//			struct stIndexPage *pstFirstLevelAdress = cFirstLevel[nWordLen];

			if (pcString == &pcBorders[sc_nBufferMemory - nWordLen])
			{
				// �������� ����� ������
				if (strcpy_s(pcBorders, sc_nBufferMemory - nWordLen, pcString) != 0)
				{
					printf("String copy failed\n");
					return;
				}

				pcStartRead = pcBorders + nWordLen;
				nRealBufferSize = sc_nBufferMemory - nWordLen;
				break;
			}
			else
			{
				if (!search(pcString, nWordLen, &(cFirstLevel[nWordLen]) , &pstSecondLevelAdress, &ppcThirdLevelAdress, &pstLastestCreatedIndexPage))
				{
					paste(&(cFirstLevel[nWordLen]), &pstSecondLevelAdress, &ppcThirdLevelAdress, pcString, &pstLastestCreatedIndexPage, &pstLastestPage);
					nUnicWords++;
				}

//				printf("%s %d\n", pcString, nWordLen);
				nTotalWords++;
			}



			pcString = strtok_s(NULL, s_cDelim, &pcTmp);
		}
	} while (blRetryFlag);

	printf("���-�� ���� �����: %d, ����������: %d\n", nTotalWords, nUnicWords);
	getchar();

	fclose(pFile);
	free(pcBorders);
}
